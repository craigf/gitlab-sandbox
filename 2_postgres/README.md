# Postgres cluster sandbox

A sandbox for postgres clusters managed by patroni. This stage is still WIP.

## How to use this

In the interest of being quick and dirty, read through this document and
copy-paste snippets of shell script as instructed. Eventually this might evolve
into something proper like ansible roles.

## Prerequisites

The environments "shared" and "postgres" in `../1_terraform/environments` are
provisioned. You should have 3 machines that will run postgres, an ssh bastion
host, and Cloud NAT.

## Installation

As root, on each server:

```
# Postgres
curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list
apt-get update
apt-get install -y postgresql-12

# Patroni
apt-get install -y build-essential python3 virtualenv
mkdir -p /opt/patroni
virtualenv --python=python3 /opt/patroni
/opt/patroni/bin/pip3 install --upgrade pip
/opt/patroni/bin/pip3 install psycopg2-binary patroni[consul]

# Consul
apt-get install -y unzip
wget https://releases.hashicorp.com/consul/1.6.2/consul_1.6.2_linux_amd64.zip
unzip ./consul_1.6.2_linux_amd64.zip
mv consul /usr/bin/
rm consul_1.6.2_linux_amd64.zip

cat <<EOF > /lib/systemd/system/consul.service
[Unit]
Description=consul
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/consul agent -server -data-dir /data/consul
Restart=always
RestartSec=5
TimeoutStartSec=0
StartLimitInterval=0

[Install]
WantedBy=multi-user.target
EOF

# Re-initialise database on persistent disk
systemctl stop postgresql@12-main.service
rm -rf /var/lib/postgresql/12/main
mkdir -p /data/postgres
chown postgres:postgres /data/postgres
ln -s /data/postgres /var/lib/postgresql/12/main
sudo -u postgres /usr/lib/postgresql/12/bin/initdb -D /var/lib/postgresql/12/main
systemctl start postgresql@12-main.service
```

Bootstrap the consul cluster. On each server:

```
mkdir /data/consul
consul agent -server -data-dir /data/consul -bootstrap-expect 3
```

In another terminal on one of the servers:

```
consul join <ip1> <ip2> <ip3>
```

The IPs are the private (static) IPs of each postgres node.

Kill all of the consul servers, then start the consul service on each node:

```
systemctl enable consul
systemctl start consul
```
