# Install GitLab

This part is simple, as this repository doesn't yet offer non-colocated
components: follow the instructions on https://about.gitlab.com/install/#ubuntu
to install gitlab-ee.

`/var/opt/gitlab` should already be a mountpoint for the persistent disk.

Run `terraform output ips` in the `../1_terraform/environments/gitlab`
directory. For convention, let's use the first instance in that list as the
primary. You should be able to ssh to any of the instances using your unix
username.

For the Let's Encrypt step, set `EXTERNAL_URL` to `https://gitlab.<ip>.xip.io`.

## Wal-e (WIP)

See https://gitlab.com/gitlab-com/runbooks/blob/master/howto/using-wale-gpg.md

```
apt-get install -y python3 lzop pv virtualenv daemontools
mkdir -p /opt/wal-e /etc/wal-e.d/env
virtualenv --python=python3 /opt/wal-e
/opt/wal-e/bin/pip3 install --upgrade pip
/opt/wal-e/bin/pip3 install boto azure wal-e
```
