# GitLab Sandbox

**Superseded by https://gitlab.com/craigfurman/infrastructure-playground**

Infrastructure as code for setting up a small instance of GitLab for testing.
There are many like this but this one is mine.

Early WIP. The goal is to test out [Geo
replication](https://about.gitlab.com/solutions/geo/).

Go through the directories in numerical order, and read the READMEs.
