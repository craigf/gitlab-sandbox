output "ips" {
  value = module.gitlab_with_geo.public_ips
}

output "deployment" {
  value = module.gitlab_with_geo.deployment
}
