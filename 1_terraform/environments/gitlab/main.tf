terraform {
  backend "gcs" {}
}

provider "google" {
  version = "~> 2.20.0"
}

provider "local" {
  version = "~> 1.4"
}

provider "random" {
  version = "~> 2.2"
}

module "gitlab_with_geo" {
  source = "../../modules/instances"

  boot_disk_size       = 30
  data_disk_mountpoint = "/var/opt/gitlab"
  data_disk_size       = var.data_disk_size
  instance_count       = 2
  machine_type         = "n1-standard-2"
  preemptible          = true
  public_static_ip     = true
  region               = var.region
  service              = "gitlab"
  ssh_public_key       = var.ssh_public_key
  ssh_username         = var.ssh_username
  subnetwork_link      = data.google_compute_subnetwork.default.self_link
  tags                 = ["gitlab"]
  use_pet_names        = true
}

resource "google_compute_firewall" "allow_web" {
  allow {
    protocol = "tcp"
    ports    = [80, 443]
  }

  name          = "allow-web"
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["gitlab"]
}

data "google_compute_subnetwork" "default" {
  name   = "default"
  region = var.region
}
