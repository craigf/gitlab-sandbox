variable "region" {}
variable "ssh_public_key" {}
variable "ssh_username" {}

variable "nat_ip_count" {
  type    = number
  default = 1
}
