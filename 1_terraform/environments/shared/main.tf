terraform {
  backend "gcs" {}
}

provider "google" {
  version = "~> 2.20.0"
}

provider "local" {
  version = "~> 1.4"
}

provider "random" {
  version = "~> 2.2"
}

module "bastion" {
  source = "../../modules/instances"

  boot_disk_size   = 10
  instance_count   = 1
  machine_type     = "f1-micro"
  preemptible      = true
  public_static_ip = true
  region           = var.region
  service          = "bastion"
  ssh_public_key   = var.ssh_public_key
  ssh_username     = var.ssh_username
  subnetwork_link  = data.google_compute_subnetwork.default.self_link
  tags             = ["bastion"]
  use_pet_names    = true
}

data "google_compute_network" "default" {
  name = "default"
}

data "google_compute_subnetwork" "default" {
  name   = "default"
  region = var.region
}
