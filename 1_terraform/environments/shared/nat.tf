resource "google_compute_router_nat" "nat" {
  name                               = "default-${var.region}"
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = google_compute_address.nat_ips.*.self_link
  region                             = var.region
  router                             = google_compute_router.nat_router.name
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    filter = "ERRORS_ONLY"
    enable = false
  }
}

resource "google_compute_router" "nat_router" {
  name    = "default-${var.region}-nat"
  network = data.google_compute_network.default.self_link
  region  = var.region
}

resource "google_compute_address" "nat_ips" {
  count = var.nat_ip_count

  name   = format("nat-default-%s-%02d", var.region, count.index + 1)
  region = var.region
}
