terraform {
  backend "gcs" {}
}

provider "google" {
  version = "~> 2.20.0"
}

provider "local" {
  version = "~> 1.4"
}

provider "random" {
  version = "~> 2.2"
}

module "postgres_cluster" {
  source = "../../modules/instances"

  data_disk_size    = var.data_disk_size
  instance_count    = 3
  machine_type      = "n1-standard-1"
  preemptible       = true
  private_static_ip = true
  region            = var.region
  service           = "postgres"
  ssh_public_key    = var.ssh_public_key
  ssh_username      = var.ssh_username
  subnetwork_link   = data.google_compute_subnetwork.default.self_link
  tags              = ["postgres"]
}

data "google_compute_subnetwork" "default" {
  name   = "default"
  region = var.region
}
