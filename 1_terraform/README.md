# terraform

Provisions Google Cloud resources using terraform.

The `environments` directory contains various terraform root modules that
correspond to different sandbox environments. They shouldn't depend on each
other, but can depend on modules.

## Prerequisites

Terraform Backend (Google Cloud Storage):

1. A GCS bucket exists to hold the Terraform state.
1. You are logged into the `gloud` CLI.
1. You have application default credentials configured (`gcloud auth
   application-default login`).
1. Your GCP user has
   [permission](https://cloud.google.com/storage/docs/access-control/iam-roles)
   to read and write objects in the bucket (storage object admin).

Google Cloud:

1. A Google project exists, to hold the generated resources. This doesn't have
   to be the same as the project holding the state bucket.

`cp .envrc.template .envrc` and fill in the values corresponding to your
environment.

Install the correct version of Terraform, specified in .terraform-version. I use
[tfenv](https://github.com/tfutils/tfenv) for this.

## Usage

1. `cd environments/<environment>`, depending on what you are provisioning.
1. `source .envrc`. I use [`direnv`](https://github.com/direnv/direnv) to
   automate this.
1. First time: `../../tf-init`. Subsequent runs in this directory can simply use
   `terraform init`, as the backend config will be saved in `.teraform`.
1. `terraform plan` and/or `terraform apply`.
