output "public_ips" {
  value = google_compute_address.public_static_ip.*.address
}

output "deployment" {
  value = random_pet.deployment.id
}
