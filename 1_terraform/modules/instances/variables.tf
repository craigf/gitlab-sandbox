variable "instance_count" {
  type = number
}

variable "boot_disk_type" {
  default = "pd-standard"
}

variable "boot_disk_size" {
  type    = number
  default = 10
}

variable "data_disk_type" {
  default = "pd-standard"
}

# Set to non-zero to provision data disk
variable "data_disk_size" {
  default = 0
  type    = number
}

variable "data_disk_mountpoint" {
  default = "/data"
}

variable "tags" {
  type = list(string)
}

variable "private_static_ip" {
  type    = bool
  default = false
}

variable "public_static_ip" {
  type    = bool
  default = false
}

variable "preemptible" {
  type    = bool
  default = false
}

variable "use_pet_names" {
  type        = bool
  default     = false
  description = "use memorable names for instances instead of IDs"
}

variable "machine_type" {}
variable "region" {}
variable "service" {}
variable "ssh_public_key" {}
variable "ssh_username" {}
variable "subnetwork_link" {}
