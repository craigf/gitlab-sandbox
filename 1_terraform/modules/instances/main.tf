resource "google_compute_instance" "instances" {
  count = var.instance_count

  name = format("%s-%s", var.service, module.names.names[count.index])

  allow_stopping_for_update = true
  machine_type              = var.machine_type
  tags                      = var.tags
  zone                      = data.google_compute_zones.available_zones.names[count.index]

  dynamic "attached_disk" {
    for_each = var.data_disk_size > 0 ? [0] : []
    content {
      source      = google_compute_disk.data[count.index].self_link
      device_name = "data"
    }
  }

  boot_disk {
    initialize_params {
      image = data.google_compute_image.ubuntu.self_link
      size  = var.boot_disk_size
      type  = var.boot_disk_type
    }
  }

  network_interface {
    subnetwork = var.subnetwork_link
    network_ip = var.private_static_ip ? google_compute_address.private_static_ip[count.index].address : ""

    dynamic "access_config" {
      for_each = var.public_static_ip ? [0] : []
      content {
        nat_ip = google_compute_address.public_static_ip[count.index].address
      }
    }
  }

  labels = {
    deployment = random_pet.deployment.id
    origin     = "terraform"
    service    = var.service
  }

  metadata = {
    data-disk-mountpoint = var.data_disk_mountpoint
    data-disk-name       = "data"
    ssh-keys             = "${var.ssh_username}:${var.ssh_public_key}"
    startup-script       = data.local_file.startup_script.content
  }

  scheduling {
    automatic_restart = ! var.preemptible
    preemptible       = var.preemptible
  }
}

resource "google_compute_disk" "data" {
  count = var.data_disk_size > 0 ? var.instance_count : 0

  name = format("%s-%s-data", var.service, module.names.names[count.index])
  size = var.data_disk_size
  type = var.data_disk_type
  zone = data.google_compute_zones.available_zones.names[count.index]

  labels = {
    deployment = random_pet.deployment.id
    origin     = "terraform"
    service    = var.service
  }
}

# TODO add labels when field out of beta:
# https://www.terraform.io/docs/providers/google/r/compute_address.html#labels
resource "google_compute_address" "private_static_ip" {
  count = var.private_static_ip ? var.instance_count : 0

  address_type = "INTERNAL"
  name         = format("%s-%s-private", var.service, module.names.names[count.index])
  region       = var.region
  subnetwork   = var.subnetwork_link
}

resource "google_compute_address" "public_static_ip" {
  count = var.public_static_ip ? var.instance_count : 0

  address_type = "EXTERNAL"
  name         = format("%s-%s", var.service, module.names.names[count.index])
  region       = var.region
}

module "names" {
  source = "../names"

  name_count    = var.instance_count
  use_pet_names = var.use_pet_names
}

# Deployments can always be pets
resource "random_pet" "deployment" {
}

data "local_file" "startup_script" {
  filename = "${path.module}/bootstrap.sh"
}

data "google_compute_zones" "available_zones" {
  region = var.region
}

data "google_compute_image" "ubuntu" {
  family  = "ubuntu-1804-lts"
  project = "gce-uefi-images"
}
