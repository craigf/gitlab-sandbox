variable "name_count" {
  type = number
}

variable "use_pet_names" {
  type        = bool
  default     = false
  description = "use memorable names for instances instead of IDs"
}
