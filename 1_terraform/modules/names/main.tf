resource "random_id" "names" {
  count       = var.use_pet_names ? 0 : var.name_count
  byte_length = 3
}

resource "random_pet" "names" {
  count = var.use_pet_names ? var.name_count : 0
}
