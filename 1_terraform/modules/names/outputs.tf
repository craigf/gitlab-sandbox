output "names" {
  value = var.use_pet_names ? random_pet.names.*.id : random_id.names.*.hex
}
